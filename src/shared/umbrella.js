export const UMBRELLA = [
    {
    id: 0,
    name:'Personal Umbrella',
    image: 'assets/images/umbrella.jpg',
    category: 'appetizer',
    label:'New',
    price:'1.99',
    featured: true,
    description:'Increase Liability limits of your underlying policies'
    }
]