import React from 'react';
import { Card, CardImg, CardText, CardBody,CardTitle, CardSubtitle,Button} from 'reactstrap';
//import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody, Row, Col, Label,CardSubtitle } from 'reactstrap';
// import { Link } from 'react-router-dom';



function RenderCard({item}) {

    return(
        
        <Card>
            <CardImg  className="card-img-top img-fluid"  src={item.image} alt={item.name} />
            <CardBody>
            <CardTitle ><h3>{item.name}</h3></CardTitle>
            
            {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
            <CardText>{item.description}</CardText>
            
             <Button  size="large" color="primary">
                    <span className="fa fa-pencil fa-lg"> Click here</span>
                </Button>
            </CardBody>
        </Card>
    );

}

function Home(props) {
    return(
        <div className="container">
            <div className="row align-items-start">
            

                  <div className="col-12 col-md m-1">
                    <RenderCard item={props.personalauto} />
                </div>
                <div className="col-12 col-md m-1">
                    <RenderCard item={props.umbrella} />
                </div>
                <div className="col-12 col-md m-1">
                    <RenderCard item={props.homeowner} />
                </div> 
                {/* <div className="col-12 col-md m-1">
                    <RenderCard item={props.renter} />
                </div> */}
            </div>
        </div>
    );
}

export default Home;


