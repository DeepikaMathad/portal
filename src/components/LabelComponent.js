import React, { Component } from 'react';
import {Form,Input,Row,Col, FormGroup,Button } from 'reactstrap';

import {SCREENS} from '../shared/question';
import Question from './QuestionComponent';
class Label extends Component{

    render () {
               return(
                  
            <div>
                  <Question />
                   <br/>
                  <br/>
               {SCREENS.map((quest)=>{  
                  return (
                     <Form>
                     <FormGroup row>   
                                                    
                         {quest.fields.map((field)=> {                                       
                           if(field.AnswerType == 'Text')
                             return (<Col md={3}>                                                    
                             <Input type="Text" id={field.name} placeholder={field.Label} width="15px"/>                                     
                             </Col>)                                       
                          })}
                  

                     </FormGroup>                         
                 </Form>  
       
                  );
               })}
            </div>
        );
    }

}
export default Label;