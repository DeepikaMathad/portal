import React, { Component } from 'react';
import {Form,Input,Row,Col, FormGroup,Button } from 'reactstrap';

import {SCREENS} from '../shared/question';

class Question extends Component{

    render () {
               return(
            <div>
                
                {SCREENS.map((quest)=>{
                     if(quest.id == 0)
                    return (
                   <div className='container' style={{display:'-ms-inline-flexbox', justifyContent:'left', alignItems:'left', height: '30vh'}}>
                      <h1>{quest.HeaderQuestion}</h1>                     
                   </div>
                     );
                    
                  })}
            </div>
        );
    }

}
export default Question;